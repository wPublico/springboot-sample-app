FROM java:8-jre
MAINTAINER Prueba <dianalmtzleon@gmail.com>
ADD ./target/ /app/
CMD ["java", "-Xmx200m", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app/app.jar"]

